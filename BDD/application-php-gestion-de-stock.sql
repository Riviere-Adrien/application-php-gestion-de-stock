-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 10 déc. 2020 à 12:44
-- Version du serveur :  10.4.16-MariaDB
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `application-php-gestion-de-stock`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `ID_Site` int(254) NOT NULL,
  `ID_Article` int(254) NOT NULL,
  `Emplacement` text NOT NULL,
  `Categorie` text NOT NULL,
  `Quantite` int(254) NOT NULL,
  `Reference` text NOT NULL,
  `S_N` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table article';

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`ID_Site`, `ID_Article`, `Emplacement`, `Categorie`, `Quantite`, `Reference`, `S_N`) VALUES
(1, 1, '4890456498498', 'Matériaux de construction', 42, 'Planches de bois 14x5', ''),
(1, 45, '44dz4d', 'Fruit', 4, 'Banane', '');

-- --------------------------------------------------------

--
-- Structure de la table `entreprises`
--

CREATE TABLE `entreprises` (
  `ID_Entreprise` int(254) NOT NULL,
  `Nom` text NOT NULL,
  `Activite` text NOT NULL,
  `SIRET` int(254) NOT NULL,
  `Courriel` text NOT NULL,
  `Telephone` int(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table pour les Entreprises';

--
-- Déchargement des données de la table `entreprises`
--

INSERT INTO `entreprises` (`ID_Entreprise`, `Nom`, `Activite`, `SIRET`, `Courriel`, `Telephone`) VALUES
(1, 'Exodata', 'Autre', 2147483647, 'help@exodata.fr', 262500700),
(2, 'SDIS', 'Autre', 404561408, 'standard@sdis974.re', 262789654),
(3, 'XMS Informatique', 'Autre', 489409804, 'accueil@xms.com', 262123456),
(45, 'Restaurant les Bons Enfants', 'restauration', 897409874, 'accueil@lebon.fr', 692457896);

-- --------------------------------------------------------

--
-- Structure de la table `sites`
--

CREATE TABLE `sites` (
  `ID_Entreprise` int(254) NOT NULL,
  `ID_Site` int(254) NOT NULL,
  `Adresse` text NOT NULL,
  `GPS` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table Sites';

--
-- Déchargement des données de la table `sites`
--

INSERT INTO `sites` (`ID_Entreprise`, `ID_Site`, `Adresse`, `GPS`) VALUES
(1, 1, '4 Rue Emile Hugot', '097812560');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `ID_Entreprise` int(254) NOT NULL,
  `ID_Utilisateur` int(254) NOT NULL,
  `Type` text NOT NULL DEFAULT 'Utilisateur',
  `Nom` text NOT NULL,
  `Prenom` text NOT NULL,
  `Courriel` text NOT NULL,
  `Telephone` int(254) NOT NULL,
  `Fonction` text NOT NULL,
  `Appartenance` text NOT NULL,
  `Login` text NOT NULL,
  `Mdp` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table utilisateurs';

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`ID_Entreprise`, `ID_Utilisateur`, `Type`, `Nom`, `Prenom`, `Courriel`, `Telephone`, `Fonction`, `Appartenance`, `Login`, `Mdp`) VALUES
(3, 2, 'Utilisateur', 'Riviere', 'Eude', 'stagiaire@xms.com', 692420069, 'Magasinier Chef', 'SI Interne', 'jeaneude', '81dc9bdb52d04dc20036dbd8313ed055'),
(1, 37000456, 'Administrateur', 'Martin', 'Thomas', 't.martin@rt-iut.re', 692547703, 'Magasinier Chef', 'SI Interne', 'tma', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`ID_Article`),
  ADD KEY `ID_Site` (`ID_Site`);

--
-- Index pour la table `entreprises`
--
ALTER TABLE `entreprises`
  ADD PRIMARY KEY (`ID_Entreprise`);

--
-- Index pour la table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`ID_Site`),
  ADD KEY `ID_Entreprise` (`ID_Entreprise`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`ID_Utilisateur`),
  ADD KEY `ID_Entreprise` (`ID_Entreprise`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`ID_Site`) REFERENCES `sites` (`ID_Site`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_ibfk_1` FOREIGN KEY (`ID_Entreprise`) REFERENCES `entreprises` (`ID_Entreprise`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `utilisateurs_ibfk_1` FOREIGN KEY (`ID_Entreprise`) REFERENCES `entreprises` (`ID_Entreprise`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
