<?php

session_start();

?>	
<html>
<head>

	<title> Stock/Destock </title>

    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    
    
    <!-- Css -->
    <link href=".././Mise_en_forme/accueil.css" rel="stylesheet">
    
    <!--  Javascript -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    

</head>

<body>

<!-- Challenge et affichage Administrateur  -->
	<?php if ( $_SESSION['Type']  == "Administrateur"  ) { ?>
	    
	      <div class="d-flex" id="wrapper">

		<!-- Sidebar -->
		<div class="bg-light border-right" id="sidebar-wrapper">
			<div class="sidebar-heading">Menu gestion de stock</div>
			<div class="list-group list-group-flush">
				<a
					href="/Projet/application-php-gestion-de-stock/DRE/ajout_utilisateur.php"
					class="list-group-item list-group-item-action bg-light">Ajouter un
					utilisateur</a> <a
					href="/Projet/application-php-gestion-de-stock/DRE/suppression_utilisateur.php"
					class="list-group-item list-group-item-action bg-light">Suppression
					d'un utilisateur</a> <a
					href="/Projet/application-php-gestion-de-stock/DRE/liste_utilisateur.php"
					class="list-group-item list-group-item-action bg-light">Modification
					d'un utilisateur</a> <a href="/Projet/application-php-gestion-de-stock/TMA/ajout_entreprise.php"
					class="list-group-item list-group-item-action bg-light">Ajouter une
					entreprise</a> <a href="/Projet/application-php-gestion-de-stock/TMA/suppression_entreprise.php"
					class="list-group-item list-group-item-action bg-light">Suppression
					d'une entreprise</a> <a href="/Projet/application-php-gestion-de-stock/TMA/liste_entreprise.php"
					class="list-group-item list-group-item-action bg-light">Modification
					d'une entreprise</a>
			</div>
		</div>
		<!-- /#sidebar-wrapper -->


		<!-- Page Content -->
		<div id="page-content-wrapper">

			<nav
				class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
				<button class="btn btn-primary" id="menu-toggle">Menu</button>

				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				
				
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0"> </ul>
<form action=".././DRE/recherche_nav.php" method='POST'>
    		<input type="search" name="recherche"> <input type="submit" name="s_recherche" value="Rechercher">
    	</form>
    	</div>
    		

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
						<li class="nav-item active"><a class="nav-link" href="/Projet/application-php-gestion-de-stock/TMA/accueil.php">Accueil<span
								class="sr-only">(current)</span></a></li>
						<li class="nav-item active"><a class="nav-link"
							href="/Projet/application-php-gestion-de-stock/TMA/logout.php">Déconnexion<span
								class="sr-only">(current)</span></a></li>
					</ul>
				</div>
			</nav>
			

	<!-- Menu Toggle Script -->
	<script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
  	     
	<?php } ?> 


<!-- Challenge et affichage Magasinier chef  -->

	<?php if ( $_SESSION['Type']  == "Utilisateur" && $_SESSION['Fonction']  == "Magasinier Chef" ) { ?>
	    
	      <div class="d-flex" id="wrapper">

		<!-- Sidebar -->
		<div class="bg-light border-right" id="sidebar-wrapper">
			<div class="sidebar-heading">Menu gestion de stock</div>
			<div class="list-group list-group-flush">
				<a
					href="/Projet/application-php-gestion-de-stock/ARI/ajout_site.php"
					class="list-group-item list-group-item-action bg-light">Ajouter un
					site</a> <a
					href="/Projet/application-php-gestion-de-stock/ARI/suppression_site.php"
					class="list-group-item list-group-item-action bg-light">Suppression
					d'un site</a> <a
					href="/Projet/application-php-gestion-de-stock/ARI/liste_site.php"
					class="list-group-item list-group-item-action bg-light">Modification
					d'un site</a> <a
					href="/Projet/application-php-gestion-de-stock/TMA/ajout_article.php"
					class="list-group-item list-group-item-action bg-light">Ajouter un
					article</a> <a
					href="/Projet/application-php-gestion-de-stock/TMA/suppression_article.php"
					class="list-group-item list-group-item-action bg-light">Suppression
					d'un article</a> <a
					href="/Projet/application-php-gestion-de-stock/TMA/liste_article.php"
					class="list-group-item list-group-item-action bg-light">Modification
					d'un article</a>
					
			</div>
		</div>
		<!-- /#sidebar-wrapper -->


		<!-- Page Content -->
		<div id="page-content-wrapper">

			<nav
				class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
				<button class="btn btn-primary" id="menu-toggle">Menu</button>

				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>


				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0"> </ul>
<form action=".././DRE/recherche_nav.php" method='POST'>
    		<input type="search" name="recherche"> <input type="submit" name="s_recherche" value="Rechercher">
    	</form>
    	</div>
    		

    		
    		
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
						<li class="nav-item active"><a class="nav-link" href="/Projet/application-php-gestion-de-stock/TMA/accueil.php">Accueil<span
								class="sr-only">(current)</span></a></li>
						<li class="nav-item active"><a class="nav-link"
							href="/Projet/application-php-gestion-de-stock/TMA/logout.php">Déconnexion<span
								class="sr-only">(current)</span></a></li>
					</ul>
				</div>
			</nav>

		

	<!-- Menu Toggle Script -->
	<script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
	     
	<?php } ?>



<!-- Challenge et affichage Magasinier  -->
	<?php if ( $_SESSION['Type']  == "Utilisateur" && $_SESSION['Fonction']  == "Magasinier" ) { ?>
	    
	      <div class="d-flex" id="wrapper">

		<!-- Sidebar -->
		<div class="bg-light border-right" id="sidebar-wrapper">
			<div class="sidebar-heading">Menu gestion de stock</div>
			<div class="list-group list-group-flush">
				<a
					href="/Projet/application-php-gestion-de-stock/TMA/ajout_article.php"
					class="list-group-item list-group-item-action bg-light">Ajouter un
					article</a> <a
					href="/Projet/application-php-gestion-de-stock/TMA/suppression_article.php"
					class="list-group-item list-group-item-action bg-light">Supression
					d'un article</a> <a
					href="/Projet/application-php-gestion-de-stock/TMA/liste_article.php"
					class="list-group-item list-group-item-action bg-light">Modification
					d'un article</a>
					
			</div>
		</div>
		<!-- /#sidebar-wrapper -->


		<!-- Page Content -->
		<div id="page-content-wrapper">

			<nav
				class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
				<button class="btn btn-primary" id="menu-toggle">Menu</button>

				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0"> </ul>
<form action=".././DRE/recherche_nav.php" method='POST'>
    		<input type="search" name="recherche"> <input type="submit" name="s_recherche" value="Rechercher">
    	</form>
    	</div>
    		

    		

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
						<li class="nav-item active"><a class="nav-link" href="/Projet/application-php-gestion-de-stock/TMA/accueil.php">Accueil<span
								class="sr-only">(current)</span></a></li>
						<li class="nav-item active"><a class="nav-link"
							href="/Projet/application-php-gestion-de-stock/TMA/logout.php">Déconnexion<span
								class="sr-only">(current)</span></a></li>
					</ul>
				</div>
			</nav>


	<!-- Menu Toggle Script -->
	<script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
  
  
	     
	<?php } ?>




<!-- Challenge et affichage prestataire -->

	<?php if ( $_SESSION['Type']  == "Utilisateur" && $_SESSION['Fonction']  == "Prestataire" ) { ?>
	    
	    <div class="d-flex" id="wrapper">

		<!-- Sidebar -->
		<div class="bg-light border-right" id="sidebar-wrapper">
			<div class="sidebar-heading">Menu gestion de stock</div>
			<div class="list-group list-group-flush">
				<a
					href="/Projet/application-php-gestion-de-stock/TMA/ajout_article.php"
					class="list-group-item list-group-item-action bg-light">Ajouter un
					article</a>
			</div>
		</div>

		<!-- /#sidebar-wrapper -->


		<!-- Page Content -->
		<div id="page-content-wrapper">

			<nav
				class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
				<button class="btn btn-primary" id="menu-toggle">Menu</button>

				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0"> </ul>
<form action=".././DRE/recherche_nav.php" method='POST'>
    		<input type="search" name="recherche"> <input type="submit" name="s_recherche" value="Rechercher">
    	</form>
    	</div>
    		

    		

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
						<li class="nav-item active"><a class="nav-link" href="/Projet/application-php-gestion-de-stock/TMA/accueil.php">Accueil<span
								class="sr-only">(current)</span></a></li>
						<li class="nav-item active"><a class="nav-link"
							href="/Projet/application-php-gestion-de-stock/TMA/logout.php">Déconnexion<span
								class="sr-only">(current)</span></a></li>
					</ul>
				</div>
			</nav>




	<!-- Menu Toggle Script -->
	<script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
	     
	<?php } ?>


   	 <!-- Message de confirmation d'action -->
        <?php
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'success') {
                echo "<script type='text/javascript'> alert('Action effectuee avec succes'); </script>";
            } else if ($_GET['action'] == 'failed') {
                echo "<script type='text/javascript'> alert('Action echouee'); </script>";
            }
        }
        ?>
       

