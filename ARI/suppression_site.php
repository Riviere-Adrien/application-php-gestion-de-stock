<?php

include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {

          
    // Inclusion des dependances
    include ("connect.php");
    include ("Site.php");

    // Requete SQL pour recuperer la liste des sites

    $requete_affichage_liste = $connexion->query("SELECT adresse FROM `sites`");

    $requete_affichage_liste->setFetchMode(PDO::FETCH_CLASS, 'Site');

    ?>
        
 	<!-- Titre de section -->
	<br>
	<h1>
		<p class="text-center">Suppression site</p>
	</h1>

      
        
        	<!-- Formulaire de suprression -->

	<form action='traitement_suppression_site.php' method='POST'>
		<br> <br>

		<div class="form-group">
			<label for="selection">Choisissez le site a supprimer</label> <select
				name="selection" class="form-control">

				<!-- Boucle permettant d afficher la requete SQL -->
        				<?php
    while ($liste = $requete_affichage_liste->fetch()) {
        ?>
        				<option value='<?php echo $liste->getAdresse();?>'
					name='selection'><?php echo $liste->getAdresse();?> </option>
        				<?php }?>
        				
        				</select> <br>
        				<input type='submit' value='Supprimer un site'
				name="submit" class="btn btn-primary">

		</div>

	</form>
	
	<a href="/Projet/application-php-gestion-de-stock/TMA/accueil.php"
		class="button">Accueil</a>



<?php

include (".././Mise_en_forme/footer.php");
} 
else {
   // header("Location: .././TMA/login.php");
    echo $_SESSION['Login'];
}
?>