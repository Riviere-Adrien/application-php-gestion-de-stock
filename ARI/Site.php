<?php

/**
 * @file      Site.php
 * @author    Adrien Riviere
 * @date      Decembre 2020
 * @brief     Contient la class Site
 */

/**
 * @class       Site
 * @brief       La déclaration de la class Site
 * @details     La classe Site permet de crée des objets site et de définir les différentes méthode pour utiliser les données
 * @author      Adrien Riviere
 */

class Site
{

    /**
     * @var mixed données correspondant a l'adresse du site
     * @private
     */
    private $adresse;
    
    /**
     * @var mixed données correspondant aux coordonnées gps du site
     * @private
     */
    private $gps;

    /**
     * @var mixed données correspondant a l'id du site
     * @private
     */
    private $id_site;

    /**
     * @var mixed données correspondant a l'id de l'entreprise auquel apartient le site
     * @private
     */
    private $id_entreprise;
    
    
    
    

    /**
     * @brief permet de récuperer la donnée ID entreprise
     * @return mixed
     */
    public function getId_entreprise()
    {
        return $this->id_entreprise;
    }

    /**
     * @brief permet de définir la donnée ID entreprise
     * @param mixed $id_entreprise
     * @return mixed
     */
    public function setId_entreprise($id_entreprise)
    {
        $this->id_entreprise = $id_entreprise;
    }

    /**
     * @brief permet de récupérer la donnée ID Site
     * @return mixed
     */
    public function getId_site()
    {
        return $this->id_site;
    }

    /**
     * @return mixed
     * @brief permet de définir la donnée ID site
     * @param mixed $id_site
     */
    public function setId_site($id_site)
    {
        $this->id_site = $id_site;
    }

    /**
     * @brief permet de récupérer la donnée Adresse
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @brief permet de récupérer la donnée Gps
     * @return mixed
     */
    public function getGps()
    {
        return $this->gps;
    }

    /**
     * @param mixed $_adresse
     * @brief permet de définir la donnée Adresse
     * @return mixed
     */
    public function setAdresse($_adresse)
    {
        $this->adresse = $_adresse;
    }

    /**
     * @param mixed $_gps
     * @brief permet de définir la donnée Gps
     * @return mixed
     */
    public function setGps($_gps)
    {
        $this->gps = $_gps;
    }

    
    /**
     * @brief permet de retourner un obet en char
     * @param mixed
     * @return string
     */
    public function __toString()
    {
        return $this->adresse;
        
    }
}
