<?php

include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {
    ?>
    
	<!-- Formulaire d'ajout d'un site -->
	
	
	<!-- Titre de section -->
	<br>
	<h1>
		<p class="text-center">Ajout site</p>
	</h1>

	

	<form action='traitement_ajout_site.php' method='POST'>
		<br> <br>
		<div class="form-group">
		 <?php
    include ("connect.php");

    $requete = $connexion->query("SELECT ID_Entreprise, Nom FROM entreprises");
    echo "Entreprise : <select name='id_entreprise' size='1'>";
    while ($resultat = $requete->fetch()) {
        echo "<option value=" . $resultat['ID_Entreprise'] . "> " . $resultat['Nom'] . "</option>";
    }
    echo "</select>";
    
    
    
    ?>			
		</div>

		<div class="form-group">
			<label for="adresse">Adresse du site</label> <input type="text"
				name="adresse" class="form-control" id="id_adresse"
				placeholder="Adresse">
		</div>
		<div class="form-group">
			<label for="gps">Coordonnes GPS</label> <input type="text" name="gps"
				class="form-control" id="id_gps" placeholder="GPS">
		</div>
		<div class="form-group">
			<label for="id_site">Numero d'identification du Site</label> <input
				type="text" name="id_site" class="form-control" id="id_site"
				placeholder="Numero d'ID du site">
		</div>

		<input type='submit' value='Ajouter un site' name="submit"
			class="btn btn-primary">
	</form>
	
	
	


	<a href="javascript:history.back()">Retour</a>



	<!-- Message de confirmation d'action -->
        <?php
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'failed') {
            echo "<script type='text/javascript'> alert('Identifiant Article : Doublon'); </script>";
        } elseif ($_GET['action'] == 'empty') {
            echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
        }
    }
    
    ?>




<?php

include (".././Mise_en_forme/footer.php");

} else {
    header("Location: .././TMA/login.php");
}
?>
