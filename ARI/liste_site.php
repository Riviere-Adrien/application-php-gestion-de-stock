<?php
include (".././Mise_en_forme/header.php");

include ("connect.php");
include ("Site.php");

if ($_SESSION['Login'] != NULL) {

    // Recuperation des donnees
    $requete = $connexion->query("SELECT entreprises.Nom, sites.ID_Site, sites.Adresse, sites.GPS FROM `sites` inner join entreprises on sites.ID_Entreprise=entreprises.ID_Entreprise");
    $recup_id = $connexion->query("SELECT ID_Site FROM sites");
    $requete1 = $requete->fetchAll();
    ?>

<!-- Titre de section -->
<br>
<h1>
	<p class="text-center">Modification site</p>
</h1>
<br>


<table class="table table-bordered">
	<thead class=thead-dark>
		<th scope="col">Entreprise</th>
		<th scope="col">ID Site</th>
		<th scope="col">Adresse</th>
		<th scope="col">GPS</th>

	</thead>
	<tbody>
    
 <?php
    // Boucle permettant affichage de la requete SQL
    // while ($requete1 = $requete->fetch())
    foreach ($requete1 as $key => $variable) {
        ?>
		
	<tr>
			<td><?php echo $requete1[$key]['Nom'];?></td>
			<td><?php echo $requete1[$key]['ID_Site'];?></td>
			<td><?php echo $requete1[$key]['Adresse'];?></td>
			<td><?php echo $requete1[$key]['GPS'];?></td>
		</tr>             
<?php
    }
    ?>
     </tbody>
</table>
<!-- Formulaire de validation de selection du site -->
<form action="modif_liste_site.php" method='POST'>
	<div class="form-group">
<?php
    echo "<strong>Site</strong> : <select name='ID_Site' size='1'>";
    while ($recup_id1 = $recup_id->fetch()) {
        echo "<option value=" . $recup_id1['ID_Site'] . ">" . $recup_id1['ID_Site'] . "</option>";
    }
    echo "</select>";
    ?>
    
    
    </div>
	<br>
	<button type="submit" class="btn btn-primary">Modifier</button>
</form>

<a href="javascript:history.back()">Retour</a>
<?php

    include (".././Mise_en_forme/footer.php");
} else {
    header("Location: .././TMA/login.php");
}

?>



<!-- Message de confirmation d'action -->
<?php
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'empty') {
        echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
    }
}
?>
