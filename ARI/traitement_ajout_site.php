<?php
session_start(); // Recuperation de la session

// Inclusion des dependances
include ("Site.php");
include ("connect.php");

// Boucle pour v�rifier la pr�sence des varibables.
if (isset($_POST['adresse']) && isset($_POST['gps']) && isset($_POST['id_site']) && isset($_POST['id_entreprise'])) {

    // recuperation des variables
    $adresse = $_POST['adresse'];
    $gps = $_POST['gps'];
    $id_site = $_POST['id_site'];
    $id_entreprise = $_POST['id_entreprise'];
    $recup_id = $connexion->query("SELECT ID_Site FROM sites where ID_Site='" . $id_site . "'");
    $verif = $recup_id->fetch();

    // Les champs du formulaire ne doivent pas �tres vides
    if ($adresse != NULL && $gps != NULL && $id_site != NULL && $id_entreprise != NULL) {

        // Si la requete n'a pas aboutit, cela signifie qu'il n'y a pas de doublons et retourne un booleen false.
        if ($verif == false) {

            // Definition de l'objet
            $site = new Site();
            $site->setAdresse($adresse);
            $site->setGps($gps);
            $site->setId_site($id_site);
            $site->setId_entreprise($id_entreprise);

            // requete SQL
            $ajout_site = $connexion->prepare("INSERT INTO sites (ID_Entreprise, ID_Site, Adresse, GPS) VALUES (:ID_Entreprise, :ID_Site, :Adresse, :Gps)");

            // Binding des param�tres � envoyer
            $ajout_site->bindParam(':ID_Entreprise', $id_entreprise);
            $ajout_site->bindParam(':ID_Site', $id_site);
            $ajout_site->bindParam(':Adresse', $adresse);
            $ajout_site->bindParam(':Gps', $gps);

            // Execution de la requete

            $ajout_site->execute();

            // Redirection vers l'acceuil avec message de confirmation
            header('Location: /Projet/application-php-gestion-de-stock/TMA/accueil.php?action=success');
        } 
        else {
            header("Location: ajout_site.php?action=failed");
        }
    } else {
        header("Location: ajout_site.php?action=empty");
    }
}
else {
    header("Location: .././TMA/login.php"); 
}
?>