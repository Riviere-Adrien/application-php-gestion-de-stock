<?php 

include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {
?>


<?php
include ("connect.php");
include ("Entreprise.php");

// Requete SQL pour recuperer la liste des entreprises

$requete_affichage_liste = $connexion->query("SELECT nom FROM `entreprises`");

$requete_affichage_liste->setFetchMode(PDO::FETCH_CLASS, 'Entreprise');

?>


<!-- Titre de section -->
	<br>
	<h1>
		<p class="text-center">Supprimer une entreprise</p>
	</h1>
	
	
	<form action='suppression_entreprise_traitement.php' method='POST'>
		<br> <br>

		<div class="form-group">
			<label for="selection">Choisissez l'entreprise a supprimer</label> <select
				name="selection" class="form-control">
				<?php
    while ($liste = $requete_affichage_liste->fetch()) {
        ?>
				<option value='<?php echo $liste->getNom();?>' name = 'selection'><?php echo $liste->getNom();?> </option>
				<?php }?>
				
				</select>
				<br> <br>
				<input type='submit' value='Supprimer une entreprise' name="submit" class="btn btn-primary">

		</div>

	</form>

	<a href="javascript:history.back()">Retour</a>

	


<?php 

include (".././Mise_en_forme/footer.php");
 
} else {
    header("Location: login.php");
}
?>