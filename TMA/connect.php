<?php

    /**
     * @file      connect.php
     * @author    MARTIN Thomas
     * @date      Decembre 2020
     * @brief     Permet la connexion a la base de donn�es avec les divers param�tre de connexion fichier a inclure dans les fichiers qui utilise la bdd
     */
    
    /**
     *
     * @var string valeur correspondant a l'adresse du serveur
     */
    $serveur = "localhost";
    
    /**
     *
     * @var string correspondant au nom de la base de donn�es
     * @public
     */
    $dbname = "application-php-gestion-de-stock";
    
    /**
     *
     * @var string valeur correspondant au nom d'utilisateur
     * @public
     */
    $user = "root";
    
    // Creation de data source name pour etablir la connexion
    
    /**
     *
     * @var string valeur permettant la requ�te de connexion
     * @brief Creation de data source name pour etablir la connexion
     * @public
     */
    $projet_php = 'mysql:host=' . $serveur . ';dbname=' . $dbname . ';port=3306;charset=utf8';
    
    // Parametres de connexion et stockes dans une variable
    
    try {
    
        /**
         * @var PDO variable permettant la connexion a la bdd
         * @public
         */
        $connexion = new PDO($projet_php, $user);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
    catch (PDOException $exception) {}

?>