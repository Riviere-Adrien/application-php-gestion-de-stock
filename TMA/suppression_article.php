<?php

include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {
    ?>

<?php
    include ("connect.php");
    include ("Article.php");

    // Requete SQL pour recuperer la liste des entreprises

    $requete_affichage_liste = $connexion->query("SELECT reference FROM `articles`");

    $requete_affichage_liste->setFetchMode(PDO::FETCH_CLASS, 'Article');

    ?>



	<br>
	<h1>
		<p class="text-center">Suppression article</p>
	</h1>


	<!-- Formulaire de suppression -->
	<form action='suppression_article_traitement.php' method='POST'>
		<br> <br>

		<div class="form-group">
			<label for="selection">Choisissez l'article a supprimer</label> <select
				name="selection" class="form-control">
				
<?php
    while ($liste = $requete_affichage_liste->fetch()) {
        ?>
				<option value='<?php echo $liste->getReference();?>'
					name='selection'><?php echo $liste->getReference();?> </option>
			<?php
    }
    ?>
				</select> <br> <input type='submit'
				value='Supprimer un article' name="submit" class="btn btn-primary">

		</div>

	</form>

	<a href="javascript:history.back()">Retour</a>



<?php

include (".././Mise_en_forme/footer.php");

} else {
    header("Location: login.php");
}
?>