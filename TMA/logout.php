<!-- A include lors de la deconnexion -->
<?php
session_start(); //Recuperation de la session
session_unset(); //Supression de toutes les variables $_SESSION
session_destroy(); //Fermeture de la session
header('location:/Projet/application-php-gestion-de-stock/TMA/login.php');
?>