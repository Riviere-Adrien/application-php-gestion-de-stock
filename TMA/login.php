<html>

<?php
/**
 * @file      login.php
 * @author    MARTIN Thomas
 * @date      Decembre 2020
 * @brief     Permet a l'utilisateur de se connecter grace a un login/mdp 
 */
?>
 
<head>

<meta charset="UTF-8">

<!-- Ajout de bootstrap et css --> 

<link href=".././Mise_en_forme/login.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

<!-- Ajout de java script --> 

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>

</head> 



<body>

  <br><br><h1><p class="text-center"> Bienvenue sur Stock/Destock</p></h1>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Connexion</h5>
            <form class="form-signin" action='login_traitement.php' method='POST'>
              <div class="form-label-group">
                <input type="text" id="utilisateur" class="form-control" name="login" placeholder="Nom d'utilisateur" required autofocus>
                <label for="login">Login</label>
              </div>
              <div class="form-label-group">
                <input type="password" id="mdp" class="form-control" name="mdp" placeholder="Mot de passe" required>
                <label for="mdp">Mot de passe</label>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" value='Se connnecter' ame="submit">Connecter</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
   <br>

      
        <?php
        
        /**
         * @brief boucle qui permet d'afficher un message d'erreur en cas d'echec d'authentification
         */
        if (isset($_GET['auth'])) {
            if ($_GET['auth'] == 'err') {
                echo "echec de l'authentification, login et/ou mot de passe incorrect";
            }
        }
        ?>
</body>
</html>

