<?php

include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {
    ?>


<!-- Titre de section -->
	<br>
	<h1>
		<p class="text-center">Ajouter une entreprise</p>
	</h1>

	
	<form action='ajout_entreprise_traitement.php' method='POST'>
		<br> <br>
		<div class="form-group">
			<label for="id_entreprise">ID Entreprise</label> <input type="number"
				class="form-control" name="id_entreprise"
				placeholder="Numero d'identification">
		</div>
		<div class="form-group">
			<label for="nom_entreprise">Nom de l'entreprise</label> <input
				type="text" class="form-control" name="nom_entreprise"
				placeholder="Entreprise">
		</div>
		<!--- Selection du secteur d'activit� parmis une liste g�n�rique -->
		<div class="form-group">
			<label for="selection">Secteur d'activite</label> <select
				name="selection" class="form-control">
				<option value='hotellerie'>Hotellerie</option>
				<option value='camping'>Terrains de camping et parcs pour caravanes
					ou vehicules de loisirs</option>
				<option value='restauration'>Restauration</option>
				<option value='cafeterias'>Cafeterias et autres libres-services</option>
				<option value='Cinematographie'>Cinematographie</option>
				<option value='transports'>Compagnie de transports</option>
				<option value='tourisme'>Loisirs / Activites touristiques</option>
				<option value='autre'>Autre</option>
			</select>
		</div>
		<div class="form-group">
			<label for="siret">Numero de SIRET</label> <input type="text"
				class="form-control" name="siret" placeholder="SIRET">
		</div>
		<div class="form-group">
			<label for="courriel">Adresse de courriel</label> <input type="email"
				class="form-control" name="courriel" placeholder="email">
		</div>
		<div class="form-group">
			<label for="telephone">Numero de telephone</label> <input type="text"
				class="form-control" name="telephone" placeholder="Telephone">
		</div>
		<input type='submit' value='Ajouter une entreprise' name="submit"
			class="btn btn-primary">
	</form>

	<a href="javascript:history.back()">Retour</a>


	<!-- Message de confirmation d'action -->
        <?php
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'failed') {
            echo "<script type='text/javascript'> alert('Identifiant Entreprise : Doublon'); </script>";
        } elseif ($_GET['action'] == 'empty') {
            echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
        }
    }
    
    include (".././Mise_en_forme/footer.php");
    
} else {
    header("Location: login.php");
}
?>
	

</body>
</html>