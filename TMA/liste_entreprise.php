<?php
include (".././Mise_en_forme/header.php");

include ("connect.php");
include ("Entreprise.php");

if ($_SESSION['Login'] != NULL) {

    // Récupération des données
    $requete = $connexion->query("SELECT * FROM entreprises");
    $recup_id = $connexion->query("SELECT ID_Entreprise FROM entreprises");

    ?>

<br>
<h1>
	<p class="text-center">Modification Entreprise</p>
</h1>
<!-- Titre de section -->


<table class="table table-bordered">
	<thead class=thead-dark>
		<tr color=#007BFF>
			<th scope="col">ID_Entreprise</th>
			<th scope="col">Nom</th>
			<th scope="col">Activite</th>
			<th scope="col">SIRET</th>
			<th scope="col">Couriel</th>
			<th scope="col">Telephone</th>
		</tr>
	</thead>
	<tbody>
   <?php

    while ($requete1 = $requete->fetch()) {

        ?>
        
         
		
			<tr>
			<td><?php echo $requete1['ID_Entreprise'];?></td>
			<td><?php echo $requete1['Nom'];?></td>
			<td><?php echo $requete1['Activite'];?></td>
			<td><?php echo $requete1['SIRET'];?></td>
			<td><?php echo $requete1['Courriel'];?></td>
			<td><?php echo $requete1['Telephone'];?></td>

		</tr>   

<?php
    }

    ?>
		</tbody>
</table>
<form action="modif_liste_entreprise.php" method='POST'>
	<div class="form-group">
		<?php
    echo "<strong>ID Entreprise</strong> : <select name='ID_Entreprise' size='1'>";
    while ($recup_id1 = $recup_id->fetch()) {
        echo "<option value=" . $recup_id1['ID_Entreprise'] . ">" . $recup_id1['ID_Entreprise'] . "</option>";
    }
    echo "</select>";
    ?>

	</div>
	<br>
	<button type="submit" class="btn btn-primary">Modifier</button>

</form>

<a href="javascript:history.back()">Retour</a>

</form>



<?php

    include (".././Mise_en_forme/footer.php");
} else {
    header("Location: login.php");
}
?>


<!-- Message de confirmation d'action -->
<?php
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'empty') {
        echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
    }
}
?>