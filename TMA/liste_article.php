<?php
include (".././Mise_en_forme/header.php");

include ("connect.php");
include ("Article.php");

if ($_SESSION['Login'] != NULL) {

    // Récupération des données
    $requete = $connexion->query("SELECT sites.Adresse, articles.ID_Article, articles.Emplacement, articles.Categorie, articles.Quantite, articles.Reference, articles.S_N FROM `articles` inner join sites on articles.ID_Site=sites.ID_Site");
    $recup_id = $connexion->query("SELECT ID_Article FROM articles");

    ?>

<br>
<h1>
	<p class="text-center">Modification Article</p>
</h1>
<table class="table table-bordered">
	<thead class=thead-dark>
		<tr color=#007BFF>
			<th scope="col">Localisation</th>
			<th scope="col">ID Article</th>
			<th scope="col">Emplacement</th>
			<th scope="col">Categorie</th>
			<th scope="col">Quantite</th>
			<th scope="col">Reference</th>
			<th scope="col">Numero de serie</th>
		</tr>
	</thead>
	<tbody>
   <?php

    while ($requete1 = $requete->fetch()) {

        ?>
			<tr>
			<td><?php echo $requete1['Adresse'];?></td>
			<td><?php echo $requete1['ID_Article'];?></td>
			<td><?php echo $requete1['Emplacement'];?></td>
			<td><?php echo $requete1['Categorie'];?></td>
			<td><?php echo $requete1['Quantite'];?></td>
			<td><?php echo $requete1['Reference'];?></td>
			<td><?php echo $requete1['S_N'];?></td>

		</tr>
	

<?php
    }

    ?>
	</tbody>
</table>
<form action="modif_liste_article.php" method='POST'>
	<div class="form-group">
		<?php
    echo "<strong>ID Article</strong> : <select name='ID_Article' size='1'>";
    while ($recup_id1 = $recup_id->fetch()) {
        echo "<option value=" . $recup_id1['ID_Article'] . ">" . $recup_id1['ID_Article'] . "</option>";
    }
    echo "</select>";
    ?>
	</div>
	<button type="submit" class="btn btn-primary">Modifier l'article</button>

</form>

<a href="javascript:history.back()">Retour</a>


</form>

<?php

    include (".././Mise_en_forme/footer.php");
} else {
    header("Location: login.php");
}
?>

<!-- Message de confirmation d'action -->
<?php
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'empty') {
        echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
    }
}
?>