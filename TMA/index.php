<?php

/**
 * @file      index.php
 * @author    MARTIN Thomas
 * @date      Decembre 2020
 * @brief     Page qui permet de rediriger vers la page de login 
 */

	if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/Projet/application-php-gestion-de-stock/TMA/login.php');
	exit;
?>

