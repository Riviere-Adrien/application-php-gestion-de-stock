<?php
session_start(); // Recuperation de la session
include ("connect.php");
include ("Entreprise.php");

// Boucle pour v�rifier la pr�sence des varibables.
if (isset($_POST['id_entreprise']) && isset($_POST['nom_entreprise']) && isset($_POST['selection']) && isset($_POST['siret']) && isset($_POST['courriel']) && isset($_POST['telephone'])) {
 
        // D�finition des variables
        $id_entreprise = $_POST['id_entreprise'];
        $entreprise = $_POST['nom_entreprise'];
        $selection = $_POST['selection'];
        $siret = $_POST['siret'];
        $courriel = $_POST['courriel'];
        $telephone = $_POST['telephone'];
        $recup_id = $connexion->query("SELECT ID_Entreprise FROM entreprises where ID_Entreprise='" . $id_entreprise . "'");
        $verif = $recup_id->fetch();

        //Les champs du formulaire ne doivent pas �tres vides
        if ($id_entreprise != NULL && $entreprise != NULL && $selection != NULL && $siret != NULL && $courriel != NULL && $telephone != NULL){
            
        // Si la requete n'a pas aboutit, cela signifie qu'il n'y a pas de doublons et retourne un booleen false.
        if ($verif == false) {
            
            // Cr�ation de l'objet entreprise
            $company = new Entreprise();
            $company->setId($id_entreprise);
            $company->setNom($entreprise);
            $company->setActivite($selection);
            $company->setSiret($siret);
            $company->setCourriel($courriel);
            $company->setTelephone($telephone);

            // Preparation de la requete SQL
            $ajout_entreprise = $connexion->prepare("INSERT INTO entreprises (ID_Entreprise, Nom, Activite, SIRET, Courriel, Telephone) VALUES (:ID_Entreprise, :Nom, :Activite, :SIRET, :Courriel, :Telephone)");

            // Association des param�tres � envoyer
            $ajout_entreprise->bindParam(':ID_Entreprise', $id_entreprise);
            $ajout_entreprise->bindParam(':Nom', $entreprise);
            $ajout_entreprise->bindParam(':Activite', $selection);
            $ajout_entreprise->bindParam(':SIRET', $siret);
            $ajout_entreprise->bindParam(':Courriel', $courriel);
            $ajout_entreprise->bindParam(':Telephone', $telephone);

            // Execution de la requete
            $ajout_entreprise->execute();

            // Redirection vers l'acceuil avec message de confirmation
            header('Location: accueil.php?action=success');
        } else {
            header("Location: ajout_entreprise.php?action=failed");
        }
    }
    else {
        header("Location: ajout_entreprise.php?action=empty");
    }
}
?>