<?php

include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {
?>


	<br>
	<h1>
		<p class="text-center">Ajouter un article</p>
	</h1>

	<!-- Formulaire d'aj out d'articlme-->
	<form action='ajout_article_traitement.php' method='POST'>
		<br>
		<br>
		<div class="form-group">
				
<?php

    include ("connect.php");
    include (".././ARI/Site.php");

    $requete = $connexion->query("SELECT ID_Site, Adresse FROM sites");

    echo "Site : <select name='ID_Site' size='1'>";
    while ($resultat = $requete->fetch()) {
        echo "<option value=" . $resultat['ID_Site'] . "> " . $resultat['Adresse'] . "</option>";
    }
    echo "</select>";
    echo "<br>";

?>
					
			
		</div>
		
		<div>
		
		<label for="categorie">ID Article</label> <input type="number"
			class="form-control" name="id_article"
			placeholder="Numero d'identification">
		</div>
		<div class="form-group">
			<label for="categorie">Emplacement</label> <input type="text"
				class="form-control" name="emplacement" placeholder="Emplacement">
		</div>
		<div class="form-group">
			<label for="categorie">Categorie</label> <input type="text"
				class="form-control" name="categorie" placeholder="Categorie">
		</div>
		<div class="form-group">
			<label for="reference">Reference</label> <input type="text"
				class="form-control" name="reference" placeholder="Reference">
		</div>
		<div class="form-group">
			<label for="quantite">Quantite</label> <input type="number"
				class="form-control" name="quantite" placeholder="Ex : 4">
		</div>
		<div class="form-group">
			<label for="serie">Numero de Serie</label> <input type="text"
				class="form-control" name="sn" placeholder="S/N">
		</div>
		<input type='submit' value='Ajouter un article' name="submit"
			class="btn btn-primary">
	</form>

	<a href="javascript:history.back()">Retour</a>

		
		
		<!-- Message de confirmation d'action -->
        <?php
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'failed') {
                echo "<script type='text/javascript'> alert('Identifiant Article : Doublon'); </script>";
            } elseif ($_GET['action'] == 'empty') {
                echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
            }
        }
        
        include (".././Mise_en_forme/footer.php");
        
} else {
    header("Location: login.php");
}
        ?>


