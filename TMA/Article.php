<?php

/**
 * @file      Article.php
 * @author    MARTIN Thomas 
 * @date      Decembre 2020
 * @brief     Contient la class Article
 */

/**
 *
 * @class       Article
 * @brief       La déclaration de la class Article
 * @details     La classe Article permet de crée des objets article et de définir les différentes méthode pour utiliser les données
 * @author      MARTIN Thomas
 *
 */

class Article
{
    /**
     * @var mixed données correspondant a l'id site auquel l'article appartient 
     * @private
     */
    private $id_site;
    
    /**
     * @var mixed données correspondant a l'id de l'article
     *  @private
     */
    private $id_article;
    
    /**
     * @var mixed données correspondant a l'emplacement de l'article
     *  @private
     */
    private $emplacement;
    
    /**
     * @var mixed données correspondant a la catégorie d'article auquel il appartient
     *  @private
     */
    private $categorie;
    
    /**
     * @var mixed données correspondant a la reference de l'article
     *  @private
     */
    private $reference;
    
    /**
     * @var mixed données correspondant a la quantité de l'article 
     *  @private
     */
    private $quantite;
    
    /**
     * @var mixed données correspondant au numéro de série de l'article
     *  @private
     */
    private $sn;

    /**
     * @brief permet de récupérer la donnée ID site
     * @return mixed
     */
    public function getId_site()
    {
        return $this->id_site;
    }

    /**
     * @brief permet de récupérer la donnée ID article
     * @return mixed
     */
    public function getId_article()
    {
        return $this->id_article;
    }

    /**
     * @brief permet de récupérer la donnée Emplacement
     * @return mixed
     */
    public function getEmplacement()
    {
        return $this->emplacement;
    }

    /**
     * @brief permet de récupérer la donnée Categorie
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @brief permet de récupérer la donnée Reference
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @brief permet de récupérer la donnée Quantite
     * @return mixed
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @brief permet de récupérer la donnée Sn
     * @return mixed
     */
    public function getSn()
    {
        return $this->sn;
    }

    /**
     * @brief permet de définir la données ID site
     * @param mixed $id_site
     */
    public function setId_site($id_site)
    {
        $this->id_site = $id_site;
    }

    /**
     * @brief permet de définir la données ID article
     * @param mixed $id_article
     */
    public function setId_article($id_article)
    {
        $this->id_article = $id_article;
    }

    /**
     * @brief permet de définir la données Emplacement
     * @param mixed $emplacement
     */
    public function setEmplacement($emplacement)
    {
        $this->emplacement = $emplacement;
    }

    /**
     * @brief permet de définir la données Categorie
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @brief permet de définir la données Reference
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @brief permet de définir la données Quantité
     * @param mixed $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }

    /**
     * @brief permet de définir la données Sn
     * @param mixed $sn
     */
    public function setSn($sn)
    {
        $this->sn = $sn;
    }

    
    /**
     * @brief permet de retourner un obet en char
     * @param mixed 
     * @return string 
     */
    public function __toString() {
        return $this->reference;
    }
    
}

?>