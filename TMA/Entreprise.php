<?php

/**
 * @file      Entreprise.php
 * @author    MARTIN Thomas
 * @date      Decembre 2020
 * @brief     Contient la class  Entreprise
 */

/**
 * @class       Entreprise
 * @brief       La déclaration de la class  Entreprise
 * @details     La classe  Entreprise permet de crée des objets entreprise et de définir les différentes méthode pour utiliser les données
 * @author      MARTIN Thomas
 */

class Entreprise 
{

    /**
     * @var mixed données correspondant a l'id de l'entreprise 
     *  @private
     */
    private $id;

    /**
     * @var mixed données correspondant au nom de l'entreprise
     *  @private
     */
    private $nom; 
    
    /**
     * @var mixed données correspondant au type d'activité de l'entreprise 
     *  @private
     */
    private $activite; 
    
    /**
     * @var mixed données correspondant au numéro de siret de l'entreprise 
     *  @private
     */
    private $siret; 
    
    /**
     * @var mixed données correspondant a l'adresse mail de l'entreprise 
     *  @private
     */
    private $courriel;  
    
    /**
     * @var mixed données correspondant au numero de téléphone de l'entreprise
     *  @private
     */
    private $telephone; 
    
    /**
     * @brief permet de récupérer la donnée ID
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @brief permet de récupérer la donnée Nom
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @brief permet de récupérer la donnée Activite
     * @return mixed
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * @brief permet de récupérer la donnée Siret
     * @return mixed
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @brief permet de récupérer la donnée Courriel
     * @return mixed
     */
    public function getCourriel()
    {
        return $this->courriel;
    }

    /**
     * @brief permet de récupérer la donnée Telephone
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @brief permet de definir la donnée ID 
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @brief permet de definir la donnée Nom
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @brief permet de definir la donnée Activite
     * @param mixed $activite
     */
    public function setActivite($activite)
    {
        $this->activite = $activite;
    }

    /**
     * @brief permet de definir la donnée Siret
     * @param mixed $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @brief permet de definir la donnée Courriel
     * @param mixed $courriel
     */
    public function setCourriel($courriel)
    {
        $this->courriel = $courriel;
    }

    /**
     * @brief permet de definir la donnée Telephone
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }
        
    
    /**
     * @brief permet de retourner un obet en char
     * @param mixed
     * @return string
     */
    public function __toString() {
        return $this->nom;
    }

}

?>