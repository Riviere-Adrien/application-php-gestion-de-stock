<?php
session_start(); // Recuperation de la session
include ("connect.php");
include ("Article.php");


// Boucle pour v�rifier la pr�sence des varibables.
if (isset($_POST['ID_Site']) && isset($_POST['id_article']) && isset($_POST['emplacement']) && isset($_POST['categorie']) && isset($_POST['reference']) && isset($_POST['quantite']) && isset($_POST['sn'])) {

    // D�finition des variables
    $id_site = $_POST['ID_Site'];
    $id_article = $_POST['id_article'];
    $emplacement = $_POST['emplacement'];
    $category = $_POST['categorie'];
    $reference = $_POST['reference'];
    $quantite = $_POST['quantite'];
    $sn = $_POST['sn'];
    $recup_id = $connexion->query("SELECT ID_Article FROM articles where ID_Article='" . $id_article . "'");
    $verif = $recup_id->fetch();

    // Les champs du formulaire ne doivent pas �tres vides
    if ($id_article != NULL && $emplacement != NULL && $category != NULL && $reference != NULL && $quantite != NULL) {

        // Si la requete n'a pas aboutit, cela signifie qu'il n'y a pas de doublons et retourne un booleen false.
        if ($verif == false) {

            // Cr�ation de l'objet entreprise
            $article = new Article();
            $article->setId_site($id_site);
            $article->setId_article($id_article);
            $article->setEmplacement($emplacement);
            $article->setCategorie($category);
            $article->setReference($reference);
            $article->setQuantite($quantite);
            $article->setSn($sn);

            // Preparation de la requete SQL
            $ajout_article = $connexion->prepare("INSERT INTO articles (ID_Site, ID_Article, Emplacement, Categorie, Quantite, Reference, S_N) VALUES (:ID_Site, :ID_Article, :Emplacement, :Categorie, :Quantite, :Reference, :S_N)");

            // Association des param�tres � envoyer
            $ajout_article->bindParam(':ID_Site', $id_site);
            $ajout_article->bindParam(':ID_Article', $id_article);
            $ajout_article->bindParam(':Emplacement', $emplacement);
            $ajout_article->bindParam('Categorie', $category);
            $ajout_article->bindParam(':Quantite', $quantite);
            $ajout_article->bindParam(':Reference', $reference);
            $ajout_article->bindParam(':S_N', $sn);

            // Execution de la requete
            $ajout_article->execute();

            // Redirection vers l'acceuil avec message de confirmation
            header('Location: accueil.php?action=success');
     
        } 
        else {
            header("Location: ajout_article.php?action=failed");
        }
        
    }
    else {
        header("Location: ajout_article.php?action=empty");
    }
}

 else {
    header("Location: .././TMA/login.php");
}

?>
