<?php session_start(); ?> // Ouverture de la session
<?php
include ("connect.php");
include ("recherche.php");


// Entreprise 


if (isset($_POST["s_entreprise"]) and $_POST["s_entreprise"] == "Rechercher") {
    $_POST["terme_entreprise"] = htmlspecialchars($_POST["terme_entreprise"]); // pour sécuriser le formulaire contre les intrusions html
    $terme_entreprise = $_POST["terme_entreprise"];
    $terme_entreprise = trim($terme_entreprise); // pour supprimer les espaces dans la requête de l'internaute
    $terme_entreprise = strip_tags($terme_entreprise); // pour supprimer les balises html dans la requête
}

if (isset($terme_entreprise)) {
    $terme_entreprise = strtolower($terme_entreprise);
    $select_terme_entreprise = $connexion->prepare("SELECT Nom, Activite, SIRET, Courriel, Telephone  FROM entreprises WHERE Nom LIKE ? OR Activite LIKE ? OR SIRET LIKE ? OR Courriel LIKE ? OR Telephone LIKE ?");
    $select_terme_entreprise->execute(array( "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%"));
    
    $resultat_entreprise = $select_terme_entreprise->fetchAll();
    
    echo '<br>';
    echo "<table>";
    
    foreach ($resultat_entreprise as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Nom : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Nom'] . "</td>";
        echo "<tr>";
        echo '<td><b> Activite : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Activite'] . "</td>";
        echo "<tr>";
        echo '<td><b> SIRET : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['SIRET'] . "</td>";
        echo "<tr>";
        echo '<td><b> Courriel : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Courriel'] . "</td>";
        echo "<tr>";
        echo '<td><b> Telephone : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Telephone'] . "</td>";
    }
    
    if ($resultat_entreprise != NULL)
    
    {
      $resultat = $resultat_entreprise; 
    }
}

else

{
    $message = "Pas de recherche d'entreprise demandé ";
}



//Site 


if (isset($_POST["s_site"]) and $_POST["s_site"] == "Rechercher") {
    $_POST["terme_site"] = htmlspecialchars($_POST["terme_site"]); // pour sécuriser le formulaire contre les intrusions html
    $terme_site = $_POST["terme_site"];
    $terme_site = trim($terme_site); // pour supprimer les espaces dans la requête de l'internaute
    $terme_site = strip_tags($terme_site); // pour supprimer les balises html dans la requête
}

if (isset($terme_site))

{
    $terme_site = strtolower($terme_site);
    $select_terme_site = $connexion->prepare("SELECT Adresse, GPS FROM sites WHERE Adresse LIKE ? OR GPS LIKE ? ");
    $select_terme_site->execute(array( "%" . $terme_site . "%", "%" . $terme_site . "%"));
    
    $resultat_site = $select_terme_site->fetchAll();
    
    echo '<br>';
    echo "<table>";
  
    
    foreach ($resultat_site as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Adresse : </b></td>';
        echo "<td>" . $resultat_site[$key]['Adresse'] . "</td>";
        echo "<tr>";
        echo '<td><b> GPS : </b></td>';
        echo "<td>" . $resultat_site[$key]['GPS'] . "</td>";
        
    
}

if ($resultat_site != NULL)

{
    $resultat = $resultat_site;
}

}

else

{
    $message = "Pas de recherche de site demandé";
}





// Article 



// permet de verifier les informations entré dans la recherche

if (isset($_POST["s_article"]) and $_POST["s_article"] == "Rechercher") {
    $_POST["terme_article"] = htmlspecialchars($_POST["terme_article"]); // pour sécuriser le formulaire contre les intrusions html
    $terme_article = $_POST["terme_article"];
    $terme_article = trim($terme_article); // pour supprimer les espaces dans la requête de l'internaute
    $terme_article = strip_tags($terme_article); // pour supprimer les balises html dans la requête
}

if (isset($terme_article)) 
    
    {
    $terme_article = strtolower($terme_article);
    $select_terme_article = $connexion->prepare("SELECT Emplacement, Categorie, Reference, S_N FROM articles WHERE Emplacement LIKE ? OR Categorie LIKE ? OR Reference LIKE ? OR S_N LIKE ?");
    $select_terme_article->execute(array( "%" . $terme_article . "%", "%" . $terme_article . "%", "%" . $terme_article . "%", "%" . $terme_article . "%"));
    
    $resultat_article = $select_terme_article->fetchAll();
    
    echo '<br>';
    echo "<table>";
    
    
    foreach ($resultat_article as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Emplacement : </b></td>';
        echo "<td>" . $resultat_article[$key]['Emplacement'] . "</td>";
        echo "<tr>";
        echo '<td><b> Categorie : </b></td>';
        echo "<td>" . $resultat_article[$key]['Categorie'] . "</td>";
        echo "<tr>";
        echo '<td><b> Reference : </b></td>';
        echo "<td>" . $resultat_article[$key]['Reference'] . "</td>";
        echo "<tr>";
        echo '<td><b> S_N : </b></td>';
        echo "<td>" . $resultat_article[$key]['S_N'] . "</td>";
    }
    
    if ($resultat_article != NULL)
    
    {
        $resultat = $resultat_article;
    }
    
    } 
    
else 

    {
    $message = "Pas de recherche d'article demandé";
    }

   

    
    
    
 // Utilisateur 
 
    

    
    if (isset($_POST["s_utilisateur"]) and $_POST["s_utilisateur"] == "Rechercher") {
        $_POST["terme_utilisateur"] = htmlspecialchars($_POST["terme_utilisateur"]); // pour sécuriser le formulaire contre les intrusions html
        $terme_utilisateur = $_POST["terme_utilisateur"];
        $terme_utilisateur = trim($terme_utilisateur); // pour supprimer les espaces dans la requête de l'internaute
        $terme_utilisateur = strip_tags($terme_utilisateur); // pour supprimer les balises html dans la requête
    }
    
    if (isset($terme_utilisateur))
    
    {
        $terme_utilisateur = strtolower($terme_utilisateur);
        $select_terme_utilisateur = $connexion->prepare("SELECT Type, Nom, Prenom, Courriel, Telephone, Fonction, Appartenance, Login, Mdp FROM utilisateurs WHERE Nom LIKE ? OR Prenom LIKE ? OR Courriel LIKE ? OR Telephone LIKE ? OR Fonction LIKE ? OR Appartenance LIKE ? OR Login LIKE ? OR Mdp LIKE ?");
        $select_terme_utilisateur->execute(array("%" . $terme_utilisateur . "%","%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%"));
        
        $resultat_utilisateur = $select_terme_utilisateur->fetchAll();
        
        echo '<br>';
        echo "<table>";

        foreach ($resultat_utilisateur as $key => $variable) {
            echo "<tr>";
            echo '<td><b> Type : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Type'] . "</td>";
            echo '<td><b> Type : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Type'] . "</td>";
            echo "<tr>";
            echo '<td><b> Nom : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Nom'] . "</td>";
            echo "<tr>";
            echo '<td><b> Prenom : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Prenom'] . "</td>";
            echo "<tr>";
            echo '<td><b> Courriel : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Courriel'] . "</td>";
            echo "<tr>";
            echo '<td><b> Telephone : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Telephone'] . "</td>";
            echo "<tr>";
            echo '<td><b> Fonction : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Fonction'] . "</td>";
            echo "<tr>";
            echo '<td><b> Appartenance : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Appartenance'] . "</td>";
            echo "<tr>";
            echo '<td><b> Login : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Login'] . "</td>";
            echo "<tr>";
            echo '<td><b> Mdp : </b></td>';
            echo "<td>" . $resultat_utilisateur[$key]['Mdp'] . "</td>";
        }
        
        if ($resultat_utilisateur != NULL)
        
        {
            $resultat = $resultat_utilisateur;
        }
        
    }
    
    else
    
    {
        $message = "Pas de recherche d'utilisateur demandé";
    }
    
    
    // variable recherché stocker dans resultat 
    
?>
