<?php

/**
 * @file      Utilisateur.php
 * @author    REVEL Dorian
 * @date      Decembre 2020
 * @brief     Contient la class Utilisateur
 */

/**
 * @class       Utilisateur
 * @brief       La déclaration de la class Utilisateur
 * @details     La classe Utilisateur permet de crée des objets utilisateur et de définir les différentes méthode pour utiliser les données
 * @author      REVEL Dorian
 */

class Utilisateur 
{
    
    /**
     * @var mixed données correspondant a l'id entreprise auquel apartient l'utilisateur 
     * @private
     */
    private $id_entreprise;

    /**
     * @var mixed données correspondant a l'id de l'utilisateur 
     * @private
     */
    private $id_utilisateur;
    
    /**
     * @var mixed données correspondant au type d'utilisateur 
     * @private
     */
    private $type;
    
    /**
     * @var mixed données correspondant au nom de l'utilisateur 
     * @private
     */
    private $nom;
    
    /**
     * @var mixed données correspondant au prenom de l'utilisateur
     * @private
     */
    private $prenom;
    
    /**
     * @var mixed données correspondant a l'adresse mail de l'utilisateur
     * @private
     */
    private $mail;
    
    /**
     * @var mixed données correspondant au téléphone de l'utilisateur
     * @private
     */
    private $telephone;
    
    /**
     * @var mixed données correspondant a la fonction de l'utilisateur
     * @private
     */
    private $fonction;
    
    /**
     * @var mixed données correspondant a l'appartenance de l'utilisateur
     * @private
     */
    private $selection;
    
    /**
     * @var mixed données correspondant au login de l'utilisateur 
     * @private
     */
    private $login;
    
    /**
     * @var mixed données correspondant au mdp de l'utilisateur
     * @private
     */
    private $mdp;
    
 
    /**
     * @brief permet de récupérer la donnée ID entreprise
     * @return mixed
     */
    public function getId_entreprise()
    {
        return $this->id_entreprise;
    }
    
    /**
     * @brief permet de récupérer la donnée ID utilisateur
     * @return mixed
     */
    public function getId_utilisateur()
    {
        return $this->id_utilisateur;
    }

    /**
     * @brief permet de récupérer la donnée Type
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @brief permet de récupérer la donnée Nom
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @brief permet de récupérer la donnée Prénom
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @brief permet de récupérer la donnée Mail
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @brief permet de récupérer la donnée Téléphone
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @brief permet de récupérer la donnée Fonction
     * @return mixed
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * @brief permet de récupérer la donnée Selection
     * @return mixed
     */
    public function getSelection()
    {
        return $this->selection;
    }

    /**
     * @brief permet de récupérer la donnée Login
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @brief permet de récupérer la donnée Mdp
     * @return mixed
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * @param mixed $id_entreprise
     * @brief permet de définir la donnée ID entreprise
     * @return mixed
     */
    public function setId_entreprise($id_entreprise)
    {
        $this->id_entreprise = $id_entreprise;
    }
    
    /**
     * @param mixed $id_utilisateur
     * @brief permet de définir la donnée ID utilisateur
     * @return mixed
     */
    public function setId_utilisateur($id_utilisateur)
    {
        $this->id_utilisateur = $id_utilisateur;
    }
    
    /**
     * @param mixed $type
     * @brief permet de définir la donnée Type
     * @return mixed
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $nom
     * @brief permet de définir la donnée Nom
     * @return mixed
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param mixed $prenom
     * @brief permet de définir la donnée Prénom
     * @return mixed
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @param mixed $mail
     * @brief permet de définir la donnée Mail
     * @return mixed
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @param mixed $telephone
     * @brief permet de définir la donnée Téléphone
     * @return mixed
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @param mixed $fonction
     * @brief permet de définir la donnée Fonction
     * @return mixed
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;
    }

    /**
     * @param mixed $selection
     * @brief permet de définir la donnée Sélection
     * @return mixed
     */
    public function setSelection($selection)
    {
        $this->selection = $selection;
    }

    /**
     * @param mixed $login
     * @brief permet de définir la donnée Login
     * @return mixed
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $mdp
     * @brief permet de définir la donnée Mdp
     * @return mixed
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;
    }   
    
    /**
     * @brief permet de retourner un obet en char
     * @param mixed
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }

}