<?php session_start(); ?> // Ouverture de la session
<?php
include ("connect.php");
include ("recherche_site.php");


//Site


if (isset($_POST["s_site"]) and $_POST["s_site"] == "Rechercher") {
    $_POST["terme_site"] = htmlspecialchars($_POST["terme_site"]); // pour sécuriser le formulaire contre les intrusions html
    $terme_site = $_POST["terme_site"];
    $terme_site = trim($terme_site); // pour supprimer les espaces dans la requête de l'internaute
    $terme_site = strip_tags($terme_site); // pour supprimer les balises html dans la requête
}

if (isset($terme_site))

{
    $terme_site = strtolower($terme_site);
    $select_terme_site = $connexion->prepare("SELECT Adresse, GPS FROM sites WHERE Adresse LIKE ? OR GPS LIKE ? ");
    $select_terme_site->execute(array( "%" . $terme_site . "%", "%" . $terme_site . "%"));
    
    $resultat_site = $select_terme_site->fetchAll();
    
    echo '<br>';
    echo "<table>";
    
    
    foreach ($resultat_site as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Adresse : </b></td>';
        echo "<td>" . $resultat_site[$key]['Adresse'] . "</td>";
        echo "<tr>";
        echo '<td><b> GPS : </b></td>';
        echo "<td>" . $resultat_site[$key]['GPS'] . "</td>";
        
        
    }
    
    if ($resultat_site != NULL)
    
    {
        $resultat = $resultat_site;
    }
    
}

else

{
    $message = "Pas de recherche de site demandé";
}

?>