<?php session_start(); ?> // Ouverture de la session
<?php
include ("connect.php");
include ("recherche_utilisateur.php");



// Utilisateur




if (isset($_POST["s_utilisateur"]) and $_POST["s_utilisateur"] == "Rechercher") {
    $_POST["terme_utilisateur"] = htmlspecialchars($_POST["terme_utilisateur"]); // pour sécuriser le formulaire contre les intrusions html
    $terme_utilisateur = $_POST["terme_utilisateur"];
    $terme_utilisateur = trim($terme_utilisateur); // pour supprimer les espaces dans la requête de l'internaute
    $terme_utilisateur = strip_tags($terme_utilisateur); // pour supprimer les balises html dans la requête
}

if (isset($terme_utilisateur))

{
    $terme_utilisateur = strtolower($terme_utilisateur);
    $select_terme_utilisateur = $connexion->prepare("SELECT Type, Nom, Prenom, Courriel, Telephone, Fonction, Appartenance, Login, Mdp FROM utilisateurs WHERE Nom LIKE ? OR Prenom LIKE ? OR Courriel LIKE ? OR Telephone LIKE ? OR Fonction LIKE ? OR Appartenance LIKE ? OR Login LIKE ? OR Mdp LIKE ?");
    $select_terme_utilisateur->execute(array( "%" . $terme_utilisateur . "%","%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%", "%" . $terme_utilisateur . "%"));
    
    $resultat_utilisateur = $select_terme_utilisateur->fetchAll();
    
    echo '<br>';
    echo "<table>";
    
    foreach ($resultat_utilisateur as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Type : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Type'] . "</td>";
        echo "<tr>";
        echo '<td><b> Nom : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Nom'] . "</td>";
        echo "<tr>";
        echo '<td><b> Prenom : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Prenom'] . "</td>";
        echo "<tr>";
        echo '<td><b> Courriel : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Courriel'] . "</td>";
        echo "<tr>";
        echo '<td><b> Telephone : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Telephone'] . "</td>";
        echo "<tr>";
        echo '<td><b> Fonction : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Fonction'] . "</td>";
        echo "<tr>";
        echo '<td><b> Appartenance : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Appartenance'] . "</td>";
        echo "<tr>";
        echo '<td><b> Login : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Login'] . "</td>";
        echo "<tr>";
        echo '<td><b> Mdp : </b></td>';
        echo "<td>" . $resultat_utilisateur[$key]['Mdp'] . "</td>";
    }
    
    if ($resultat_utilisateur != NULL)
    
    {
        $resultat = $resultat_utilisateur;
    }
    
}

else

{
    $message = "Pas de recherche d'utilisateur demandé";
}


// variable recherché stocker dans resultat

?>
