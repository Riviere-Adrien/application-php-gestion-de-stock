
<?php

include (".././Mise_en_forme/header.php");


include ("connect.php");

?>

<div class="container-fluid">

<!-- Titre de section -->
<br>
<h1>
<p class="text-center">Recherche</p>
</h1>


<?php 


if (isset($_POST["s_recherche"]) and $_POST["s_recherche"] == NULL )
{
    header("location : .././TMA/accueil.php ");
}


if (isset($_POST["s_recherche"]) and $_POST["s_recherche"] == "Rechercher") {
    $_POST["recherche"] = htmlspecialchars($_POST["recherche"]); // pour sécuriser le formulaire contre les intrusions html
    $recherche = $_POST["recherche"];
    $recherche = trim($recherche); // pour supprimer les espaces dans la requête de l'internaute
    $recherche = strip_tags($recherche); // pour supprimer les balises html dans la requête
}

if (isset($recherche)) {
    
    
    $recherche = strtolower($recherche);
    $select_recherche = $connexion->prepare("SELECT Nom, Activite, SIRET, Courriel, Telephone  FROM entreprises WHERE Nom LIKE ? OR Activite LIKE ? OR SIRET LIKE ? OR Courriel LIKE ? OR Telephone LIKE ?");
    $select_recherche->execute(array( "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%"));
    
    $resultat_recherche = $select_recherche->fetchAll();
    
}
    
    echo '<br>';
    echo "<table>";
    
    foreach ($resultat_recherche as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Nom : </b></td>';
        echo "<td>" . $resultat_recherche[$key]['Nom'] . "</td>";
        echo "<tr>";
        echo '<td><b> Activite : </b></td>';
        echo "<td>" . $resultat_recherche[$key]['Activite'] . "</td>";
        echo "<tr>";
        echo '<td><b> SIRET : </b></td>';
        echo "<td>" . $resultat_recherche[$key]['SIRET'] . "</td>";
        echo "<tr>";
        echo '<td><b> Courriel : </b></td>';
        echo "<td>" . $resultat_recherche[$key]['Courriel'] . "</td>";
        echo "<tr>";
        echo '<td><b> Telephone : </b></td>';
        echo "<td>" . $resultat_recherche[$key]['Telephone'] . "</td>";
        echo '<br>';
    }
    
    
    if ($resultat_recherche != NULL)
    
    {
        $resultat = $resultat_recherche;
    }
     
   
 
else 

    {
    $message = "Pas de recherche demandé";
    }
    
    
    

    if (isset($recherche)) {
    
    $select_recherche1 = $connexion->prepare("SELECT ID_Site, Adresse, GPS FROM sites WHERE ID_Site LIKE ? OR Adresse LIKE ? OR GPS LIKE ? ");
    $select_recherche1->execute(array( "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%"));
    
    $resultat_recherche1 = $select_recherche1->fetchAll();
    
    
    }
    
    echo '<br>';
    echo "<table>";
    
    
    foreach ($resultat_recherche1 as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Site : </b></td>';
        echo "<td>" . $resultat_recherche1[$key]['Site'] . "</td>";
        echo "<tr>";
        echo '<td><b> Adresse : </b></td>';
        echo "<td>" . $resultat_recherche1[$key]['Adresse'] . "</td>";
        echo "<tr>";
        echo '<td><b> GPS : </b></td>';
        echo "<td>" . $resultat_recherche1[$key]['GPS'] . "</td>";
        echo '<br>';
        
        
    }
    
    if ($resultat_recherche1!= NULL)
    
    {
        $resultat1 = $resultat_recherche1;
    }
    
    
    
    if (isset($recherche)) {
        
    
    $select_recherche2 = $connexion->prepare("SELECT Emplacement, Categorie, Reference, S_N FROM articles WHERE Emplacement LIKE ? OR Categorie LIKE ? OR Reference LIKE ? OR S_N LIKE ?");
    $select_recherche2->execute(array( "%" . $recherche  . "%", "%" . $recherche  . "%", "%" . $recherche  . "%", "%" . $recherche  . "%"));
    
    $resultat_recherche2 = $select_recherche2->fetchAll();
    
    
    }
    
    echo '<br>';
    echo "<table>";
    
    
    foreach ($resultat_recherche2 as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Emplacement : </b></td>';
        echo "<td>" . $resultat_recherche2[$key]['Emplacement'] . "</td>";
        echo "<tr>";
        echo '<td><b> Categorie : </b></td>';
        echo "<td>" . $resultat_recherche2[$key]['Categorie'] . "</td>";
        echo "<tr>";
        echo '<td><b> Reference : </b></td>';
        echo "<td>" . $resultat_recherche2[$key]['Reference'] . "</td>";
        echo "<tr>";
        echo '<td><b> S_N : </b></td>';
        echo "<td>" . $resultat_recherche2[$key]['S_N'] . "</td>";
        echo '<br>';
    }
    
    
    if ($resultat_recherche2 != NULL)
    
    {
        $resultat2 = $resultat_recherche2;
    }
    
    
    if (isset($recherche)) {
    
    $select_recherche3 = $connexion->prepare("SELECT Type, Nom, Prenom, Courriel, Telephone, Fonction, Appartenance, Login FROM utilisateurs WHERE Type LIKE ? OR Nom LIKE ? OR Prenom LIKE ? OR Courriel LIKE ? OR Telephone LIKE ? OR Fonction LIKE ? OR Appartenance LIKE ? OR Login LIKE ? ");
    $select_recherche3->execute(array("%" . $recherche . "%","%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%", "%" . $recherche . "%"));
    
    $resultat_recherche3 = $select_recherche3->fetchAll(); 
    
    }
    
    
    echo '<br>';
    echo "<table>";
    
    foreach ($resultat_recherche3  as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Type : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Type'] . "</td>";
        echo "<tr>";
        echo '<td><b> Nom : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Nom'] . "</td>";
        echo "<tr>";
        echo '<td><b> Prenom : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Prenom'] . "</td>";
        echo "<tr>";
        echo '<td><b> Courriel : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Courriel'] . "</td>";
        echo "<tr>";
        echo '<td><b> Telephone : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Telephone'] . "</td>";
        echo "<tr>";
        echo '<td><b> Fonction : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Fonction'] . "</td>";
        echo "<tr>";
        echo '<td><b> Appartenance : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Appartenance'] . "</td>";
        echo "<tr>";
        echo '<td><b> Login : </b></td>';
        echo "<td>" . $resultat_recherche3[$key]['Login'] . "</td>";
        echo "<tr>";
        echo '<td><b></b></td>';
        echo '<br>';
    }
    
    
    
    if ($resultat_recherche3 != NULL)
    
    {
        $resultat3 = $resultat_recherche3;
    }
    
    if ($resultat_recherche == NULL && $resultat_recherche1 == NULL && $resultat_recherche2 == NULL && $resultat_recherche3 == NULL)
    
    {
        echo "<script type='text/javascript'> alert('Pas de résultat trouvé'); </script>";
     
    }
    
 ?> 	
 
 </div> 
 <?php 
 
 
    
    include (".././Mise_en_forme/footer.php");

?>
