<?php session_start(); ?> // Ouverture de la session
<?php

include ("connect.php");
include ("recherche_entreprise.php");


// Entreprise 


if (isset($_POST["s_entreprise"]) and $_POST["s_entreprise"] == "Rechercher") {
    $_POST["terme_entreprise"] = htmlspecialchars($_POST["terme_entreprise"]); // pour sécuriser le formulaire contre les intrusions html
    $terme_entreprise = $_POST["terme_entreprise"];
    $terme_entreprise = trim($terme_entreprise); // pour supprimer les espaces dans la requête de l'internaute
    $terme_entreprise = strip_tags($terme_entreprise); // pour supprimer les balises html dans la requête
}

if (isset($terme_entreprise)) {
    $terme_entreprise = strtolower($terme_entreprise);
    $select_terme_entreprise = $connexion->prepare("SELECT Nom, Activite, SIRET, Courriel, Telephone  FROM entreprises WHERE Nom LIKE ? OR Activite LIKE ? OR SIRET LIKE ? OR Courriel LIKE ? OR Telephone LIKE ?");
    $select_terme_entreprise->execute(array( "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%", "%" . $terme_entreprise . "%"));
    
    $resultat_entreprise = $select_terme_entreprise->fetchAll();
    
    echo '<br>';
    echo "<table>";
    
    foreach ($resultat_entreprise as $key => $variable) {
        echo "<tr>";
        echo '<td><b> Nom : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Nom'] . "</td>";
        echo "<tr>";
        echo '<td><b> Activite : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Activite'] . "</td>";
        echo "<tr>";
        echo '<td><b> SIRET : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['SIRET'] . "</td>";
        echo "<tr>";
        echo '<td><b> Courriel : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Courriel'] . "</td>";
        echo "<tr>";
        echo '<td><b> Telephone : </b></td>';
        echo "<td>" . $resultat_entreprise[$key]['Telephone'] . "</td>";
    }
    
    if ($resultat_entreprise != NULL)
    
    {
      $resultat = $resultat_entreprise; 
    }
}

else

{
    $message = "Pas de recherche d'entreprise demandé ";
}

?>