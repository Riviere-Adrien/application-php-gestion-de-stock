<?php


include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {
    ?>


<!-- ajout du framwork boostrap -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>


<!-- definir un conteneur qui prend tout l'ecran -->
<div class="container-fluid">

	<!-- Titre de section -->
	<br>
	<h1>
		<p class="text-center">Ajout utilisateur</p>
	</h1>
	<br>
	<br>


	<!-- création du formulaire / fichier et methode d'envoie -->
	<form action="traitement_ajout_utilisateur.php" method='POST'>

		<!-- different champ du formulaire avec les different champ pour creer l'utilisateur -->

		<!-- Champ qui liste les entreprise par nom et transmet l'ID -->
		<div class="form-group">
                <?php
    include ("connect.php");
    include (".././TMA/Entreprise.php");
    
    //Affichage de la liste des entreprises
    $requete = $connexion->query("SELECT ID_Entreprise, Nom FROM entreprises");
    echo "Entreprise : <select name='id_entreprise' size='1'>";
    while ($resultat = $requete->fetch()) {
        echo "<option value=" . $resultat['ID_Entreprise'] . "> " . $resultat['Nom'] . "</option>";
    }
    echo "</select>";
    echo "<br><br>";
    ?>			
			</div>


		<!-- Autres champ plus basique pour la création de l'utilisateur -->
		<div class="form-group">
			<label for="id_entreprise">ID Utilisateur</label> <input
				type="number" class="form-control" name="id_utilisateur">
		</div>
		<div class="form-group">
			<label for="selection">Type</label> <select name="type"
				class="form-control">
				<option value='Utilisateur'>Utilisateur</option>
				<option value='Administrateur'>Administrateur</option>
			</select>
		</div>
		<div class="form-group ">
			<label for="nom">Nom</label> <input type="text" class="form-control"
				name="nom">
		</div>
		<div class="form-group ">
			<label for="prenom">Prenom</label> <input type="text"
				class="form-control" name="prenom">
		</div>
		<div class="form-group">
			<label for="mail">Adresse mail</label> <input type="email"
				class="form-control" name="mail" placeholder="name@example.com">
		</div>
		<div class="form-group">
			<label for="telephone">Telephone</label> <input type="text"
				class="form-control" name="telephone" placeholder="0000 000 000">
		</div>
		<div class="form-group">
			<label for="fonction">Fonction</label> <select name="fonction"
				class="form-control">
				<option value='Magasinier Chef'>Magasinier Chef</option>
				<option value='Magasinier'>Magasinier</option>
				<option value='Prestataire'>Prestataire</option>
			</select>
		</div>
		<div class="form-group">
			<label for="appartenance">Appartenance</label> <select
				name="appartenance" class="form-control">
				<option value='interne'>Interne a l'entreprise</option>
				<option value='externe'>Intervenant externe</option>
			</select>
		</div>
		<div class="form-group">
			<label for="login">Login</label> <input type="text"
				class="form-control" name="login" placeholder="Login">
		</div>
		<div class="form-group">
			<label for="mdp">Mot de passe</label> <input type="text"
				class="form-control" name="mdp" placeholder="Mot de passe">
		</div>
		<br>


		<!-- bouton d'envoie  -->
		<button type="submit" class="btn btn-primary">Creer</button>

	</form>

	<!-- Bouton qui renvoie a la page precedente -->
	<br>
	<a href="javascript:history.back()">Retour</a>




<!-- Message de confirmation d'action -->
<?php
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'failed') {
            echo "<script type='text/javascript'> alert('Identifiant Utilisateur : Doublon'); </script>";
        } elseif ($_GET['action'] == 'empty') {
            echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
        }
    }
    ?>
	
	<?php

	include (".././Mise_en_forme/footer.php");

} else {
    header("Location: .././TMA/login.php");
}



?>
	
