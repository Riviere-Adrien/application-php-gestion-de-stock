
<?php 

include (".././Mise_en_forme/header.php");

if ($_SESSION['Login'] != NULL) {?> 

    	
    <?php
    
        include ("connect.php");
        include ("Utilisateur.php");
        
        // Requete SQL pour recuperer la liste des sites
        $requete_affichage_liste = $connexion->query("SELECT nom FROM `utilisateurs`");
        $requete_affichage_liste->setFetchMode(PDO::FETCH_CLASS, 'Utilisateur');
        
    ?>
    
    <!-- Titre de section -->
	<br>
	<h1>
		<p class="text-center">Supression utilisateur</p>
	</h1>

    
    
    <!-- Formulaire de suprression -->
   
    <form action='traitement_suppression_utilisateur.php' method='POST'>
    	<br><br>
    	<div class="form-group">
    		<label for="selection">Choisissez l'utilisateur a supprimer : </label> <select name="selection" class="form-control">
    		<?php
                while ($liste = $requete_affichage_liste->fetch()) 
                    {?>
    					<option value='<?php echo $liste->getNom();?>' name= 'selection'><?php echo $liste->getNom();?> </option>
    				<?php
                    }?>
                    
    				</select>
    				<br>
    				<input type='submit' value='Supprimer un utilisateur' name="submit" class="btn btn-primary">
    
    		</div>

    	</form>
    	<a href="javascript:history.back()">Retour</a>
    </body>
</html>

<?php

include (".././Mise_en_forme/footer.php");

} else {
    header("Location: .././TMA/login.php");
}
?>