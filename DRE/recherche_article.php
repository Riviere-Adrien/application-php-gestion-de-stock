<!DOCTYPE html>
<?php

session_start(); // Ouverture de la session

if ($_SESSION['Login'] != NULL) {
    ?>

<html>


<!-- entêt -->
<head lang="fr">
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Recherche</title>

<!-- ajout du framwork boostrap -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">

</head>



<!-- constitution du contenu de la page -->
<body>

	<!-- ajout du framwork boostrap -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
	<!-- titre champ -->
	<br>
	<br> Recherche Article :
	<br>
	<br>

	<!-- bouton et action des info saisie -->
	<form action="traitement_recherche_article.php" method='POST'>
		<!-- envoie vers traitement recherche article -->
		<input type="search" name="terme_article"> <input type="submit"
			name="s_article" value="Rechercher">
	</form>

	<a href="/Projet/application-php-gestion-de-stock/TMA/accueil.php"
		class="button">Accueil</a>
</body>

</html>

<?php
} else {
    header("Location: .././TMA/login.php");
}
?>