<?php
include (".././Mise_en_forme/header.php");

include ("connect.php");
include ("Utilisateur.php");

if ($_SESSION['Login'] != NULL) {
    ?>

<div class="container-fluid">

	<!-- Titre de section -->
	<br>
	<h1>
		<p class="text-center">Modification utilisateur</p>
	</h1>
<table class="table table-bordered">
		<thead class=thead-dark>
			<tr color=#007BFF>
				<th scope="col">Entreprise</th>
				<th scope="col">ID Utilisateur</th>
				<th scope="col">Type</th>
				<th scope="col">Nom</th>
				<th scope="col">Prenom</th>
				<th scope="col">Couriel</th>
				<th scope="col">Telephone</th>
				<th scope="col">Login</th>
			</tr>
		</thead>

<?php
    // Récupération des données
    $requete = $connexion->query("SELECT entreprises.Nom as Nom_entreprise, utilisateurs.ID_Utilisateur, utilisateurs.Type, utilisateurs.Nom, utilisateurs.Prenom, utilisateurs.Courriel, utilisateurs.Telephone, utilisateurs.Login, utilisateurs.Mdp FROM `utilisateurs` inner join entreprises on utilisateurs.ID_Entreprise=entreprises.ID_Entreprise");
    $recup_id = $connexion->query("SELECT ID_Utilisateur FROM utilisateurs");

    // Boucle qui permet de lister l'ensemble des utilisateur
    while ($requete1 = $requete->fetch()) {
        ?> 	
           
       
		<tbody>
			<tr>
				<td><?php echo $requete1['Nom_entreprise'];?></td>
				<td><?php echo $requete1['ID_Utilisateur'];?></td>
				<td><?php echo $requete1['Type'];?></td>
				<td><?php echo $requete1['Nom'];?></td>
				<td><?php echo $requete1['Prenom'];?></td>
				<td><?php echo $requete1['Courriel'];?></td>
				<td><?php echo $requete1['Telephone'];?></td>
				<td><?php echo $requete1['Login'];?></td>
			</tr>
		
    		<?php
    }
    ?>
	</tbody>
	</table>
	<br> <br>
	<!-- champ qui transmet le numéro id utilisateur pour une eventuel modification avec un boutton -->
	<form action="modif_liste_utilisateur.php" method='POST'>
		<div class="form-group">
			<?php
    echo "<strong>ID Utilisateur</strong> : <select name='ID_Utilisateur' size='1'>";

    while ($recup_id1 = $recup_id->fetch()) {
        echo "<option value=" . $recup_id1['ID_Utilisateur'] . ">" . $recup_id1['ID_Utilisateur'] . "</option>";
    }
    echo "</select>";
    ?>
		</div>
		<button type="submit" class="btn btn-primary">Modifier</button>


	</form>

	<a href="javascript:history.back()">Retour</a>

<?php

    include (".././Mise_en_forme/footer.php");
} else {
    header("Location: .././TMA/login.php");
}
?>

<!-- Message de confirmation d'action -->
        <?php
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'empty') {
                echo "<script type='text/javascript'> alert('Veuillez completer tous les champs'); </script>";
            }
        }
        ?>