<?php
include (".././Mise_en_forme/header.php");

// inclu les fichier php necessaire au bon fonctionnement
include ("connect.php");
include ("Utilisateur.php");

// boucle qui permet d'entrer des nouvelle information pour modification utilisateur dans des champ
if (isset($_POST['ID_Utilisateur'])) {

    // recupere les information du post et requette pour les info de la table
    $ID = $_POST['ID_Utilisateur'];
    $valeur_bouton = "Modifier l'utilisateur";
    $USER = $connexion->prepare("SELECT ID_utilisateur, Type, Nom, Prenom, Courriel, Telephone, Fonction, Appartenance, Login, Mdp FROM utilisateurs WHERE ID_Utilisateur='" . $ID . "'");
    $USER->execute();
    $modif = $USER->fetch();

    // affichage des champs

    ?>

<br>

<table class="table table-bordered">
	<thead class=thead-dark>
		<tr color=#007BFF>
			<th scope="col">Type</th>
			<th scope="col">Nom</th>
			<th scope="col">Prenom</th>
			<th scope="col">Courriel</th>
			<th scope="col">Telephone</th>
		</tr>
	</thead>
	
<?php echo '<form action="traitement_modification_utilisateur.php" method="POST">' ; ?>
	<input type="hidden" name="id_utilisateur" <?php echo ' value=  "'. $modif['ID_utilisateur'] .'" ' ?> />
	<tbody>
		<tr>
			<td><label for="Type"></label> <select name="Type"
				class="form-control">
					<option value='Utilisateur'>Utilisateur</option>
			</select>
			
			<td><input type="text" name="Nom"
				<?php echo ' value=  "'. $modif['Nom'] .'" ' ?> /></td>
			<td><input type="text" name="Prenom"
				<?php echo ' value=  "'. $modif['Prenom'] .'" ' ?> /></td>
			<td><input type="text" name="Courriel"
				<?php echo ' value=  "'. $modif['Courriel'] .'" ' ?> /></td>
			<td><input type="text" name="Telephone"
				<?php echo ' value=  "'. $modif['Telephone'] .'" ' ?> /></td>
		</tr>
	</tbody>
</table>

<br>

<table class="table table-bordered">
	<thead class=thead-dark>
		<tr color=#007BFF>
			<th scope="col">Fonction</th>
			<th scope="col">Appartenance</th>
			<th scope="col">Login</th>
			<th scope="col">Mdp</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><label for="Fonction"></label> <select name="Fonction"
				class="form-control">
					<option value='Magasinier Chef'>Magasinier Chef</option>
					<option value='Magasinier'>Magasinier</option>
					<option value='Prestataire'>Prestataire</option>
			</select></td>
			<td><input type="text" name="Appartenance"
				<?php echo ' value=  "'.  $modif['Appartenance'] .'" ' ?> /></td>
			<td><input type="text" name="Login"
				<?php echo ' value=  "'.  $modif['Login'].'" ' ?> /></td>
			<td><input type="text" name="Mdp" <?php echo ' value=  "'."".'" ' ?> /></td>
		</tr>
	</tbody>
</table>

<br>
<br>

<?php

    // affichage bouton
    echo '<input type="submit" value= "' . $valeur_bouton . '" name ="modif_user"/>';
    echo '</form>';
    include (".././Mise_en_forme/footer.php");
} else {
    header("Location: .././TMA/login.php");
}

?>

<br>
<a href="javascript:history.back()">Retour</a>

