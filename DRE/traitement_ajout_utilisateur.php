
<!DOCTYPE html>
<?php session_start(); // Ouverture de la session ?>

<html>

<!-- entêt  -->

<head>

<title>Page de traitement utilisateur</title>
<meta charset="utf-8">

</head>


<!-- corp de message  -->


<body>

      <?php

    include ("connect.php");
    include ("Utilisateur.php");

    if (isset($_POST['id_entreprise']) && isset($_POST['id_utilisateur']) && isset($_POST['type']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['telephone']) && isset($_POST['mail']) && isset($_POST['fonction']) && isset($_POST['appartenance']) && isset($_POST['login']) && isset($_POST['mdp'])) {

        // Creation des variables
        $id_entreprise = $_POST['id_entreprise'];
        $id_utilisateur = $_POST['id_utilisateur'];
        $type = $_POST['type'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $telephone = $_POST['telephone'];
        $mail = $_POST['mail'];
        $fonction = $_POST['fonction'];
        $appartenance = $_POST['appartenance'];
        $login = $_POST['login'];
        $mdp = $_POST['mdp'];

        // R�aliser un hash du mot de passe
        $hash_mdp = md5($mdp);

        // Requete pour controler l'id
        $recup_id = $connexion->query("SELECT ID_Utilisateur FROM utilisateurs where ID_Utilisateur='" . $id_utilisateur . "'");
        $verif = $recup_id->fetch();

        // Les champs du formulaire ne doivent pas �tres vides
        if ($id_entreprise != NULL && $id_utilisateur != NULL && $type != NULL && $prenom != NULL && $prenom != NULL && $telephone != NULL && $mail != NULL && $fonction != NULL && $appartenance != NULL && $login != NULL && $mdp) {

            // Si la requete n'a pas aboutit, cela signifie qu'il n'y a pas de doublons et retourne un booleen false.
            if ($verif == false) {

                // Creation de l'objet utilisateur
                $user = new Utilisateur();
                $user->setId_entreprise($id_entreprise);
                $user->setId_entreprise($id_utilisateur);
                $user->setType($type);
                $user->setNom($nom);
                $user->setPrenom($prenom);
                $user->setTelephone($telephone);
                $user->setMail($mail);
                $user->setFonction($fonction);
                $user->setSelection($appartenance);
                $user->setLogin($login);
                $user->setMdp($hash_mdp);

                // Preparation de la requete SQL
                $ajout_utilisateur = $connexion->prepare("INSERT INTO utilisateurs (ID_Entreprise, ID_Utilisateur, Type, Nom, Prenom, Courriel, Telephone, Fonction, Appartenance, Login, Mdp) VALUES (:ID_Entreprise, :ID_Utilisateur, :Type, :Nom, :Prenom, :Courriel, :Telephone, :Fonction, :Appartenance, :Login, :Mdp)");

                // Association des parametres a envoyer
                $ajout_utilisateur->bindParam(':ID_Entreprise', $id_entreprise);
                $ajout_utilisateur->bindParam(':ID_Utilisateur', $id_utilisateur);
                $ajout_utilisateur->bindParam(':Type', $type);
                $ajout_utilisateur->bindParam(':Nom', $nom);
                $ajout_utilisateur->bindParam(':Prenom', $prenom);
                $ajout_utilisateur->bindParam(':Courriel', $mail);
                $ajout_utilisateur->bindParam(':Telephone', $telephone);
                $ajout_utilisateur->bindParam(':Fonction', $fonction);
                $ajout_utilisateur->bindParam(':Appartenance', $appartenance);
                $ajout_utilisateur->bindParam(':Login', $login);
                $ajout_utilisateur->bindParam(':Mdp', $hash_mdp);

                // Execution de la requete
                $ajout_utilisateur->execute();

                // Redirection vers l'acceuil avec message de confirmation
                header('Location: /Projet/application-php-gestion-de-stock/TMA/accueil.php?action=success');
            } else {
                header("Location: ajout_utilisateur.php?action=failed");
            }
        } else {
            header("Location: ajout_utilisateur.php?action=empty");
        }
    }

    ?>

    </body>

</html>